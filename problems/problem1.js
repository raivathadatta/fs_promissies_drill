/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require("fs");
const fsPromises = fs.promises;
function problem1() {
  let folderName = "random";
  const file_name = "data/lipsum_2.txt";
  let fileName = [
    `./${folderName}/random.json`,
    `./${folderName}/random2.json`,
    `./${folderName}/random3.json`,
  ];

  fsPromises
    .mkdir(folderName)
    .catch(function (err) {
      console.log(err);
    })
    .then(() => {
      fileName.forEach((index) => {
        fsPromises.writeFile(index, "utf-8", "utf-8").then(() => {
          console.log(`${index} file created succfully`);
        });
      });
    })
    .then(() => {
      fileName.forEach((index) => {
        fsPromises
          .unlink(index)
          .then(() => console.log(`${index} file deleted succfully`));
      });
    })
    .catch((err) => {
      console.log(err);
    });
}

module.exports = { problem1: problem1 };
// problem1()
