/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require("fs").promises;
const fileName = "data/lipsum_2.txt";
const upperCaseFile = "upperCase.txt";
const lowerCase = "lowerCase.txt";
const sortData = "sortData.txt";
const storeFile = "filenames.txt";
function problem2() {
  fs.readFile(fileName, "utf-8")
    .then((data) => {
      fs.writeFile(upperCaseFile, data.toUpperCase())
        .then(() => fs.writeFile(storeFile, upperCaseFile))
        .then(() => console.log("file name added"));
    })
    .then(() => {
      return fs.readFile(upperCaseFile, "utf-8").then((data) => {
        return data;
      });
    })
    .then((data) => {
      fs.writeFile(lowerCase, JSON.stringify(data.toLowerCase().split(",")));
    })
    .then(() => {
      fs.appendFile(storeFile, `,${lowerCase}`).then(() =>
        console.log("file name lower case added ")
      );
    })
    .then(() => {
      return fs.readFile(lowerCase, "utf-8");
    })
    .then((data) => JSON.parse(data))
    .then((data) => {
      fs.writeFile(sortData, JSON.stringify(data.sort()));
    })
    .then(() => {
      fs.appendFile(storeFile, `,${sortData}`);
    })
    .then(() => console.log("file name added sorted data"))
    .then(() => {
      fs.readFile(storeFile, "utf-8")
        .then((data) => data.split(","))
        .then((data) => {
          data.forEach((element) => {
            console.log(element);
            fs.unlink(element).then(() => {
              console.log(`${element} file unlinked succfully`);
            });
          });
        });
    })
    .catch((err) => {
      console.log(err);
    });
}
module.exports = problem2;
